# NLP-REACTIVE

L’objectif de ce projet est de fournir une API REST de détection d'ESN géolocalisables dans un texte

# Installation

Dépendances : Scala, SBT, Unitex.

Unitex doit etre installé pour l'utilisateur courant pour que les commandes soient accessibles.
Il doit aussi avoir été lancé au moins une fois pour récupérer les ressources pour la langue française.

Cloner le projet, et depuis la racine du dossier exécuter `sbt run`.

# Utilisation

## Traitements accessibles par l'API

- Uploader un texte `POST /text`, mettre le texte dans le body
  -> retourne 201 et l'id si créé
- Modifier un texte `PUT /text/<id>`, mettre le texte dans le body
  -> 200 si existe et modifié
- Uploader un graphe `POST /graph`, mettre le contenu du fichier grf dans le body (mode raw)
  -> retourne 201 et l'id si créé
- Modifier un graphe `PUT /graph/<id>`, mettre le contenu du fichier grf dans le body (mode raw)
  -> 200 si existe et modifié
- Appliquer un/des graphes UNITEX sur le texte `GET /search/<text_id>/<graph_id>`
  -> retourne les lignes du texte taguées contenant des ENS

# Graphe Unitex

![Graphe](./img/ENgraph.png "Graphe affiné de reconnaissance d'entités nommées")

# Test

Lancer le graphe ci-dessus sur le texte suivant: 

> La renommée de Paris n'est plus à faire.  
> Ils se dirigent vers Paris.  
> Le paquebot de Yokohama.  
> Les Catacombes de Paris.  
> Le château de Versailles


[Récupération du texte tagué avec ENS](http://localhost:8080/search/1110133016/1370222674)

La suite des tests sont des POST ou PUT, voir Traitements accessibles par l'API pour effectuer ces requêtes.

# Informations

Les textes utilisés peuvent être trouvés [ici](./storage/texts).  