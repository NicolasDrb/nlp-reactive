package IOEntity

import scala.io.Source

object EntityRecognizer {

  def getNamedEntity(filename: String): scala.collection.mutable.Set[String] = {
    val bufferedSource = Source.fromFile(filename)
    var l = scala.collection.mutable.Set[String]()
    val p = "<EN> \\w*<\\/EN>".r
    for (line <- bufferedSource.getLines) {
      val res = p.findAllIn(line).map(_.replaceAll("<EN> |<EN>|</EN>","")).toSet
      l = l ++ res
    }
    bufferedSource.close

    l
  }

  def writeSpatialNamedEntity(filename: String, result_ens: scala.collection.mutable.Set[String]): String = {
    // open the file
    val bufferedSource = Source.fromFile(filename)
    // FileWriter
    val p = "<EN> \\w*<\\/EN>".r
    var out_line = ""
    // read lines
    for (line <- bufferedSource.getLines) {
      // get the word between tags
      val res = p.findAllIn(line).map(_.replaceAll("<EN> |<EN>|</EN>","")).mkString
      // check if it's in the set
      if (result_ens.contains(res)) {
        out_line += line.replace("EN>","ENS>").replace("<ENS> ", "<ENS>") + "\n"
      }
    }
    bufferedSource.close

    out_line
  }
}
