import java.io.{BufferedWriter, File, FileWriter}

import UnitexActor.Search
import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.HttpApp
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class WebServer(unitexActor: ActorRef) extends HttpApp {

  implicit val timeout = Timeout(600 seconds)
  implicit val ec = ExecutionContext.global

  override def routes: Route = {
    pathPrefix("text") {
      // CREATE a text to be analyzed
      (pathEnd & post) {
        createText
      } ~
      // MODIFY a text to be analyzed
      (path(IntNumber) & put) { textId =>
        updateText(textId)
      }
    } ~
    pathPrefix("graph") {
      // CREATE a graph to be used for analysis
      (pathEnd & post) {
        createGraph
      } ~
      // MODIFY a graph to be used for analysis
      (path(IntNumber) & put) { graphId =>
        updateGraph(graphId)
      }
    } ~
    path("search" / IntNumber / IntNumber) { (textId, graphId) =>
      get {
        complete {
          (unitexActor ? Search(textId, graphId)).mapTo[String]
        }
      }
    }
  }

  private def createText: Route = {
    (decodeRequest & entity(as[String])) { text =>
      // decode request content if needed (compression...)

      // id creation
      val id = Math.abs(text.hashCode) // hash can be negative (int overflow), so abs to get hash *value*

      // write received content to file
      val file = new File("storage/texts/" + id.toString)
      val bw = new BufferedWriter(new FileWriter(file))
      bw.write(text)
      bw.close()

      // returns proper HTTP code with id
      complete(StatusCodes.Created -> id.toString)
    }
  }

  private def updateText(textId: Int): Route = {
    // gets an id and a text, writes in corresponding file, returns id
    val file = new File("storage/texts/" + textId.toString)

    // check file existence
    if (file.isFile)
      (decodeRequest & entity(as[String])) { text =>
        val bw = new BufferedWriter(new FileWriter(file))
        bw.write(text)
        bw.close()

        // returns proper HTTP code with id
        complete(StatusCodes.OK -> textId.toString)
      }
    else
      complete(StatusCodes.NotFound -> "No text with the specified id")
  }

  private def createGraph: Route = {
    (decodeRequest & entity(as[String])) { graph =>
      // decode request content if needed (compression...)

      // id creation
      val id = Math.abs(graph.hashCode) // hash can be negative (int overflow), so abs to get hash *value*

      // write received content to file
      val file = new File("storage/graphs/" + id.toString + ".grf")
      val bw = new BufferedWriter(new FileWriter(file))
      bw.write(graph)
      bw.close()

      // returns proper HTTP code with id
      complete(StatusCodes.Created -> id.toString)
    }
  }

  private def updateGraph(graphId: Int): Route = {
    // gets an id and a graph, writes in corresponding file, returns id
    val file = new File("storage/graphs/" + graphId.toString + ".grf")

    // check file existence
    if (file.isFile)
      (decodeRequest & entity(as[String])) { text =>
        val bw = new BufferedWriter(new FileWriter(file))
        bw.write(text)
        bw.close()

        // returns proper HTTP code with id
        complete(StatusCodes.OK -> graphId.toString)
      }
    else
      complete(StatusCodes.NotFound -> "No graph with the specified id")
  }
}
