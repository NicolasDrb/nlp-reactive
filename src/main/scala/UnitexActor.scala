import java.io.File
import IOEntity.EntityRecognizer

import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.stream.ActorMaterializer

import scala.concurrent.ExecutionContext
import sys.process._
import scala.sys.process.Process

object UnitexActor {
  final case class Search(textId: Int, graphId: Int)
}

class UnitexActor extends Actor with ActorLogging {
  import UnitexActor._

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val ec = ExecutionContext.global

  def receive: Receive = {
    case Search(textId, graphId) => sender ! search(textId, graphId)
  }

  private def search(textId: Int, graphId: Int): String = {
    // gets ids for graph and text
    val textFile = new File("storage/texts/" + textId.toString)
    val graphFile = new File("storage/graphs/" + graphId.toString + ".grf")

    if (!textFile.isFile || !graphFile.isFile)
      throw new Exception("Incorrect text or graph ID")

    // detect named entities (cache tagged text) with Unitex
    val user = "whoami".!!.trim
    val pwd = "pwd".!!.trim
    val folder = s"storage/texts/${textId.toString}_snt"

    var cmd = s"mkdir $folder"
    val folderCreated = if (new File(folder).exists) 0 else Process(cmd).!

    // test on the error value of the command
    if (folderCreated != 0)
      throw new Exception("Error when creating folder for text treatment")

    cmd = s"/home/$user/Unitex-GramLab-3.1/App/UnitexToolLogger Normalize $pwd/storage/texts/${textId.toString} -r/home/$user/workspace/Unitex-GramLab/Unitex/French/Norm.txt --output_offsets=$pwd/storage/texts/${textId.toString}_snt/normalize.out.offsets -qutf8-no-bom"
    val textNormalized = Process(cmd).!

    if (textNormalized != 0)
      throw new Exception("Error when normalizing text")

    cmd =s"/home/$user/Unitex-GramLab-3.1/App/UnitexToolLogger Tokenize $pwd/storage/texts/${textId.toString}.snt -a/home/$user/workspace/Unitex-GramLab/Unitex/French/Alphabet.txt -qutf8-no-bom"
    val tokenized = Process(cmd).!

    if (tokenized != 0)
      throw new Exception("Error when tokenizing text")

    cmd = s"/home/$user/Unitex-GramLab-3.1/App/UnitexToolLogger Grf2Fst2 $pwd/storage/graphs/$graphId.grf -y --alphabet=/home/$user/workspace/Unitex-GramLab/Unitex/French/Alphabet.txt -qutf8-no-bom"
    val grfToFst = Process(cmd).!

    if (grfToFst != 0)
      throw new Exception("Error when converting graph")

    cmd = s"/home/$user/Unitex-GramLab-3.1/App/UnitexToolLogger Locate -t$pwd/storage/texts/${textId.toString}.snt $pwd/storage/graphs/$graphId.fst2 -a/home/$user/workspace/Unitex-GramLab/Unitex/French/Alphabet.txt -L -M --all -b -Y --stack_max=1000 --max_matches_per_subgraph=200 --max_matches_at_token_pos=400 --max_errors=50 -qutf8-no-bom"
    val located = Process(cmd).!

    if (located != 0)
      throw new Exception("Error when locating named entities")

    cmd = s"/home/$user/Unitex-GramLab-3.1/App/UnitexToolLogger Concord $pwd/storage/texts/${textId}_snt/concord.ind -s12 -l40 -r55 -t -a/home/$user/workspace/Unitex-GramLab/Unitex/French/Alphabet_sort.txt --CL -qutf8-no-bom"
    val concorded = Process(cmd).!

    if (concorded != 0)
      throw new Exception("Error during concordance between result indexes and text")

    // extract entities
    val filename = "/concord.txt"
    val found_entities = EntityRecognizer.getNamedEntity(folder+filename)
    // verify entities are locations with Sparql queries
    val l = found_entities.filter(checkSparql)
    // return tagged text
    EntityRecognizer.writeSpatialNamedEntity(folder+filename, l)
  }

  private def checkSparql(entity: String): Boolean = {
    /*
    * PREFIX dbo: <http://dbpedia.org/ontology/>
    * SELECT COUNT DISTINCT ?Place_1
    * WHERE {
    *   ?Place_1 rdf:type ?Type_1 .
    *   FILTER ( ?Type_1 IN (dbo:Location, geo:SpatialThing, dbo:Place) )
    *   FILTER ( REGEX(str(?Place_1), "Versailles", 'i') )
    * }
    * LIMIT 1*/

    val place = entity.trim
    val uri = s"https://dbpedia.org/sparql?format=text%2Fcsv&query=PREFIX%20dbo%3A%20%3Chttp%3A%2F%2Fdbpedia.org%2Fontology%2F%3E%0ASELECT%20COUNT%20DISTINCT%20%3FPlace_1%0AWHERE%20%7B%0A%20%20%3FPlace_1%20rdf%3Atype%20%3FType_1%20.%0A%20%20FILTER%20(%20%3FType_1%20IN%20(dbo%3ALocation%2C%20geo%3ASpatialThing%2C%20dbo%3APlace)%20)%0A%20%20FILTER%20(%20REGEX(str(%3FPlace_1)%2C%20%22${place}%22%2C%20%27i%27)%20)%0A%7D%0ALIMIT%201"
    // makes a request to DBpedia Sparql endpoint to receive results in CSV format

    val response = scala.io.Source.fromURL(uri).mkString

    // réponse au format CSV, on vérifie que le count est 1 sur la deuxième ligne
    response.split("\n")(1).trim == "1"
  }
}
