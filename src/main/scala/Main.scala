import akka.actor.{ActorSystem, Props}

object Main extends App {
  val system = ActorSystem.create("web-server")
  val unitexActor = system.actorOf(Props[UnitexActor], "unitexActor")
  val server = new WebServer(unitexActor)

  server.startServer("localhost", 8080)
}
